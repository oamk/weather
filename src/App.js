import React from 'react';
import './App.css';
import Position from './Position';

function App() {
  return (
    <Position />
  );
}

export default App;
